#!/usr/bin/env -S sh -c 'echo "" | sed -nE "1{:l;/^[a]{98}$/{p;q};s/$/a/;b l}" | sed "s/a/\n/g" | sed -n = | sed -nf "$0" | sed \'1!G;h;$!d\' | sed 1d'
1 i No more bottles of beer on the wall.
s/.*/Take one down and pass it around,\n& bottles of beer.\n& bottles of beer on the wall,\n/
$ ! s/[^0-9]*\([0-9]\{1,2\} \).*/&\n\1bottles of beer on the wall./
s/\n1 bottles/\n1 bottle/gp
s/bottles/bottles/p