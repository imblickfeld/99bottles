#!/usr/bin/env -S sh -c 'sed -nf "$0" "$0"'
1 {
    s/.*/99 bottles of beer on the wall,/
    h
:loop
    g
    p
    s/ on.*/./p
    s/.*/Take one down and pass it around,:&/
    /:1 / {
        s/:.*//p
        g
        s/1\( .*le\)\(.*\),$/No more\1s\2./p
        q
    }
    s/:.*//p
    g
    y/1234567890/0123456789/
    /^.[0-8] /{
        s/^.//
        x
        s/^\(.\).*/\1/
        G
        s/\n//
        h
        s/,$/.\n/
        p
        b loop
    }
    /^09/ s/^.//
    /^1 / s/es/e/
    h
    s/,$/.\n/
    p
    b loop
}