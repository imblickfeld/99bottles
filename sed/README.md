# [99bottles.sed](99bottles.sed)

writes the 99 Bottles of Beer song lyrics using the `sed` stream editor language.

It creates the lyrics in 6 stages (see the Shebang of [99bottles.sed](99bottles.sed)):

```bash
echo "" | sed -nE "1 { :l; /^[a]{98}$/{p;q}; s/$/a/; b l }"
````

screates 98 times "a" (with a trailing `\n` from `echo`),

```bash
sed "s/a/\n/g"
````
converts every "a" into a new line, having 99 lines,

```bash
sed -n '='
```
converts every line to its line number (1 – 99),

```bash
sed -nf "99bottles.sed"
```
converts every line into the corresponding song verse in reverse order (1 – 99),

```bash
sed '1!G;h;$!d'
```
reverses the lyrics (99 – 1),

```bash
sed 1d
```
removes the first empty line.

# [99bottles2.sed](99bottles2.sed)

has (of course) the same result, but using a loop and does the work in one stage.

I cheated a bit [here](https://www.99-bottles-of-beer.net/language-sed-586.html) to understand how to write a loop and calculate in `sed`.

## Important commands used in the script

| Command | Description |
| --- | --- |
| `h` | copy pattern buffer to hold buffer |
| `g` | copy hold buffer to pattern buffer |
| `x` | exchange hold and pattern buf |
| `G` | append to pattern buffer |
| `p` | print pattern buffer |
| `b` *loop* | jump to label *loop* |
