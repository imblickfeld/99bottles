The runnable code is included in [99bottles.docm](99bottles.docm)[^1] and will be run after opening the document:

![Screenshot](99bottles.png "Screenshot")

[^1]: Always be careful when opening Office documents from unknown sources – especially with macros! These files might contain harmful code.