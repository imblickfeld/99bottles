Option Explicit

Private Function Bottle(count As Integer)
    If count = 1 Then
        Bottle = " bottle"
    Else
        Bottle = " bottles"
    End If
End Function

Sub NintyNineBottles()
    Dim NL As String
    Dim bottles As String
    Dim i As Integer
    Dim p As Word.Paragraph
    
    NL = Chr(11)
    bottles = ""
    For i = 99 To 1 Step -1
        bottles = bottles & i & Bottle(i) & " of beer on the wall," & NL
        bottles = bottles & i & Bottle(i) & " of beer." & NL
        bottles = bottles & "Take one down and pass it around," & NL
        If i = 1 Then
            bottles = bottles & "No more bottles"
        Else
            bottles = bottles & (i - 1) & Bottle(i - 1)
        End If
        bottles = bottles & " of beer on the wall." & vbCrLf
    Next
    With Selection
        .EndKey wdStory
        .TypeText bottles
        .End = 0
    End With
    ActiveDocument.Saved = True
End Sub
