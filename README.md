# 99 Bottles of Beer

## What is "99 Bottles of Beer"?

> "99 Bottles of Beer" is a traditional reverse counting song in the United States and Canada. It is popular to sing on road trips, as it has a very repetitive format which is easy to memorize and can take a long time when sung in full. In particular, the song is often sung by children on long school bus trips, such as class field trips, or on Scout or Girl Guide outings.
>
> The computer scientist Donald Knuth proved that the song has a complexity of O(log *N*). Numerous computer programs exist to output the lyrics to the song. This is analogous to "Hello, World!" programs, with the addition of a loop. As with "Hello, World!", this can be a practice exercise for those studying computer programming, and a demonstration of different programming paradigms dealing with looping constructs and syntactic differences between programming languages within a paradigm.
>
> ... says [Wikipedia](https://en.wikipedia.org/wiki/99_Bottles_of_Beer)

The lyrics are quite simple:

> 99 bottles of beer on the wall,\
> 99 bottles of beer.\
> Take one down and pass it around,\
> 98 bottles of beer on the wall.
>
> 98 bottles of beer on the wall,\
> 98 bottles of beer.\
> Take one down and pass it around,\
> 97 bottles of beer on the wall.
>
> 97 bottles of beer on the wall,\
> 97 bottles of beer.\
> Take one down and pass it around,\
> 96 bottles of beer on the wall.
>
> *I think you've got the point...*
>
> 2 bottles of beer on the wall,\
> 2 bottles of beer.\
> Take one down and pass it around,\
> 1 bottle of beer on the wall.
>
> 1 bottle of beer on the wall,\
> 1 bottle of beer.\
> Take one down and pass it around,\
> No more bottles of beer on the wall.

## Why Yet Another Beer Lyrics Repo?

Apart from the fact that there are already tons of source code producing the 99 bottles of beer lyrics, I wrote these just for fun. I write them when learning a new computer language or simply because I can.

## More Bottles of Beer

You may find more lyrics here:

* https://rosettacode.org/wiki/99_bottles_of_beer
* https://www.99-bottles-of-beer.net/ (unfortunately not maintained any more)

Or if you rather want to hear the song: 😉

* https://www.youtube.com/watch?v=0XY8bnLZ4Z4
* https://www.youtube.com/watch?v=xCHYR3wRQLQ