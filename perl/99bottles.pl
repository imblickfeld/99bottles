#!/usr/bin/perl
for (my $i = 99; $i > 0; ) {
    print $i, " bottle", $i == 1 ? "" : "s", " of beer on the wall,\n";
    print $i, " bottle", $i == 1 ? "" : "s", " of beer.\nTake one down and pass it around,\n";
    --$i;
    my $s = $i;
    if ($s == 0) {
        $s = "No more";
    }
    print $s, " bottle", $i == 1 ? "" : "s", " of beer on the wall.\n\n";
}