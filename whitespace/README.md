# Whitespace

For a Whitespace interpreter have a look [here](https://pypi.org/project/whitespace/)[^1], [here (online)](https://www.dcode.fr/whitespace-language) or at [the definitive collection of projects for the Whitespace programming language, including interpreters, compilers, and programs](https://github.com/wspace/corpus).

## Syntax

* S = Space
* T = Tab
* L = Line feed

| Cmd  | Parameter | Meaning |
| --   | --        | -- |
| SS   | Number L  | Push the number onto the stack |
| SLS  | -         | Duplicate the top item on the stack |
| STS  | Number L  | Copy the *n*th item on the stack (given by the argument) onto the top of the stack |
| SLT  | -         | Swap the top two items on the stack |
| SLL  | -         | Discard the top item on the stack |
| STL  | Number L  | Slide n items off the stack, keeping the top item |
| TSSS | -         | Addition |
| TSST | -         | Subtraction |
| TSSL | -         | Multiplication |
| TSTS | -         | Integer Division |
| TSTT | -         | Modulo |
| TTS  | -         | Store in heap |
| TTT  | -         | Retrieve from heap |
| LSS  | Label L   | Mark a location in the program |
| LST  | Label L   | Call a subroutine |
| LSL  | Label L   | Jump to a label |
| LTS  | Label L   | Jump to a label if the top of the stack is zero |
| LTT  | Label L   | Jump to a label if the top of the stack is negative |
| LTL  | -         | End a subroutine and transfer control back to the caller |
| LLL  | -         | End the program |
| TLSS | -         | Output the character at the top of the stack |
| TLST | -         | Output the number at the top of the stack |
| TLTS | -         | Read a character and place it in the location given by the top of the stack |
| TLTT | -         | Read a number and place it in the location given by the top of the stack |

(The Syntax is taken from the [Wikipedia](https://en.wikipedia.org/wiki/Whitespace_(programming_language)#Syntax).)

**Numbers** must be binary numbers with S(pace) represents 0 and T(ab) represents 1. The number must be terminated by a L(ine feed). The first character represents the sign of the value – S(pace) for positive and T(ab) for negative.

Characters other than `space`, `tab` and `line feed` are ignored and thus can be used for **comments**.

A **label** is used for control flow. It is a L(ine feed)-terminated sequence of `space` and `tab` characters.

## Whitespace Intermediate Language

My Whitespace code is generated from a "Whitespace Intermediate Language" file (*.wsil). It uses the Whitespace commands above but is a little bit easier to read/write:

* Any line starting with "#" is a WSIL-only comment and will be ignored.
* Any text after the first tab is assumed a comment.
* Any text before the first tab therefore contains code
* Any `space` or `line feed` within the code will be ignored and can be used to structure the code.
* Any "S" or "0" will be converted to `<space>`.
* Any "T" or "1" will be converted to `<tab>`.
* Any "L" will be converted to `<line feed>`.
* Any `<carriage return>` will be ignored.
* All other characters will be removed.

*Example:*
```
# Whitespace Intermediate Language
SS | 01 L	PUSH +1
```
This will result in:
```
   	

```

### Convert WSIL to Whitespace

```console
$ bash tools/create-ws.sh 99bottles.wsil > 99bottles.ws
```
will convert WSIL to Whitespace.

### WSIL Syntax Highlighting

To "upgrade" your Visual Studio Code and enable syntax highlighting for the *Whitespace Intermediate Language* simply copy the directory `tools/wsil` to `~/.vscode/extensions/` and restart VS Code.

![Screenshot](tools/hello-world.png "Screenshot")


[^1]: Mit diesem Interpreter wurde das Programm erfolgreich getestet.