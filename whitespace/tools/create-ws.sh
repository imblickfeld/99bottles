#!/bin/bash
# Converts WSIL to Whitespace
(cat "$1" && echo) | while IFS="" read line; do
	echo -n "$line" | grep -qe '^#' && continue
	echo -n "$line" | sed 's/\t.*//g; s/[ \r]//g; s/[S0]/S /g; s/[T1]/T\t/g; s/L/L\n/g; s/\S//g'
done
