#!/usr/bin/env node
const N = 99;
for (let i = N; i >= 0; --i) {
    let s = i == 1 ? "" : "s";
    if (i < N) {
        console.log((i == 0 ? "No more" : i) + " bottle" + s + " of beer on the wall.")
        if (i == 0)
            break;
        else
            console.log();
    }
    console.log(i + " bottle" + s + " of beer on the wall,");
    console.log(i + " bottle" + s + " of beer.");
    console.log("Take one down and pass it around,");
}