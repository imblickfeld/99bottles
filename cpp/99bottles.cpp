#include <iostream>
using namespace std;

int main() {
    const char N = 99;
    for (char i = N; i >= 0; --i) {
        char s = i == 1 ? 0 : 's';
        if (i < N) {
            if (i == 0)
                cout << "No more";
            else
                cout << +i;
            cout << " bottle" << s << " of beer on the wall." << endl;
            if (i == 0)
                break;
            cout << endl;
        }
        cout << +i << " bottle" << s << " of beer on the wall," << endl;
        cout << +i << " bottle" << s << " of beer." << endl;
        cout << "Take one down and pass it around," << endl;
    }
    return 0;
}
