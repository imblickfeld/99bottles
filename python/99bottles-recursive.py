def bottles(N, i = None):
    if (i == None):
        i = N
    s = str(i) if i > 0 else "No more"
    b = " bottle" + ("" if i == 1 else "s")
    if i < N:
        print(s + b + " of beer on the wall.")
        if i == 0:
            return
        print()
    print(s + b + " of beer on the wall,")
    print(s + b + " of beer.")
    print("Take one down and pass it around,")
    bottles(N, i - 1)

bottles(99)
