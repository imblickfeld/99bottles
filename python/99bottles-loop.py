#!/usr/bin/env python
N = 99
for i in range(N, -1, -1):
    s = str(i) if i > 0 else "No more"
    b = " bottle" + ("" if i == 1 else "s")
    if i < N:
        print(s + b + " of beer on the wall.")
        if i == 0:
            break
        print()
    print(s + b + " of beer on the wall,")
    print(s + b + " of beer.")
    print("Take one down and pass it around,")
