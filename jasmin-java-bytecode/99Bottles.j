.class public 99Bottles

.method public static bottles(II)V
    getstatic java/lang/System/out Ljava/io/PrintStream;
    dup
    astore_2    // reg2 = addr of java/lang/System/out Ljava/io/PrintStream;
    iload_0
    ifne 10
    ldc "No more bottles "
    goto 20
10: iinc 0 -1
    iload_0
    ifne 11
    ldc "1 bottle "
    goto 20
11: iinc 0 1
    iload_0
    invokevirtual java/io/PrintStream/print (I)V
    aload_2
    ldc " bottles "
20: invokevirtual java/io/PrintStream/print (Ljava/lang/String;)V

    aload_2
    iload_1
    ifeq 22
    iinc 1 -1
    iload_1
    ifeq 21
    ldc "of beer on the wall.\n"
    goto 23
21: ldc "of beer on the wall, "
    goto 23
22: ldc "of beer.\n"
23: invokevirtual java/io/PrintStream/print (Ljava/lang/String;)V
    return
.end method

.method public static main([Ljava/lang/String;)V
    bipush 99   // starting value
    istore_2    // reg2 = outer loop counter

    getstatic java/lang/System/out Ljava/io/PrintStream;
    astore_1    // reg1 = addr of java/lang/System/out Ljava/io/PrintStream;

10: iinc 2 -1
    iconst_2
    istore_3    // reg3 = inner loop counter
20: iinc 3 -1
    iload_2
    iconst_1
    iadd
    iload_3
    invokestatic 99Bottles/bottles (II)V
    iload_3
    ifne 20
    
    aload_1
    ldc "Take one down and pass it around, "
    invokevirtual java/io/PrintStream/print (Ljava/lang/String;)V
    iload_2
    iconst_2
    invokestatic 99Bottles/bottles (II)V
    aload_1
    invokevirtual java/io/PrintStream/println ()V
    
    iload_2
    ifne 10
    return
.end method
