# Jasmin / Java ByteCode Assembler

[Jasmin](https://jasmin.sourceforge.net/) is an Assembler language that assembles directly to Java class files to be understood by a Java Virtual Machine. Reworded: 8086-Assembler relates to C such as Jasmin relates to Java.

[99Bottles.class](99Bottles.class) is a pre-assembled version of [99Bottles.j](99Bottles.j) in case you want to see the result without downloading a Jasmin/ByteCode assembler.