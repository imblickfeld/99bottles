#!/usr/bin/env -S /bin/bash -c 'vim -c "source $0"'
let lyrics = ''
let @b = 99
while @b > 0
    let lyrics .= @b . " bottle" . (@b == 1 ? "" : "s") . " of beer on the wall,\n" .
        \ @b . " bottle" . (@b == 1 ? "" : "s") . " of beer.\n" .
        \ "Take one down an pass it around,\n"
    let @b = @b - 1
    if @b == 0
        let lyrics .= "No more"
    else
        let lyrics .= @b
    endif
    let lyrics .= " bottle" . (@b == 1 ? "" : "s") . " of beer on the wall.\n\n"
endwhile
exe "normal! a" . lyrics . "\<Esc>" | " append lyrics
1                                   | " goto 1st line
