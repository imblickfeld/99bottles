#!/usr/bin/env -S /bin/bash -c 'vim -c "source $0"'
let @b = 99
while @b > 0
    echo @b . " bottle" . (@b == 1 ? "" : "s") . " of beer on the wall,"
    echo @b . " bottle" . (@b == 1 ? "" : "s") . " of beer."
    echo "Take one down an pass it around,"
    let @b = @b - 1
    if @b == 0
        echo "No more"
    else
        echo @b
    endif
    echon " bottle" . (@b == 1 ? "" : "s") . " of beer on the wall.\n\n"
endwhile
