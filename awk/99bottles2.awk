#!/usr/bin/env -S sh -c '/usr/bin/awk -f "$0" /dev/zero'
BEGIN {
    RS = "\0"
    OFS = " "
}
NR == 100 {
    print "No more bottles of beer on the wall."
    exit
}
{
    n = 100 - NR
    $0 = n " bottles of beer on the wall"
    if ($0 ~ /^1 /) gsub(/s /, " ")
    if (n < 99) print $0 ".\n"
    print $0 ","
}
/on the wall/ {
    print $1, $2, $3, $4 "."
    print "Take one down and pass it around,"
}