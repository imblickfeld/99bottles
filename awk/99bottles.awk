#!/usr/bin/awk -f
BEGIN {
    N = 99
    for (i = N; i >= 0; --i) {
        s = i == 1 ? 0 : "s"
        if (i < N) {
            if (i == 0)
                printf("No more")
            else
                printf(i)
            printf(" bottle%c of beer on the wall.\n", s)
            if (i == 0)
                break
            print
        }
        printf("%d bottle%c of beer on the wall,\n", i, s);
        printf("%d bottle%c of beer.\n", i, s);
        printf("Take one down and pass it around,\n");
    }
    exit
}