# SQLite

## Linux / Bash

The first line `-- (){ :;}; cat "$0" | sqlite3 /dev/null; exit` will be ignored by SQLite. However when executed in your Bash such as 

```console
$ ./99bottles-sqlite.sql
```
or
```console
$ bash ./99bottles-sqlite.sql
```

this line will hand over the script to `sqlite3` and run it. Therefore it is a "faked" shebang, that probably only work with Bash.

## Windows

When running in Windows SQLite may need a database file to run this script. You can use any SQLite database as this script won't write anything. However, you might want to create an empty database:

```bat
copy NUL 99bottles.db
```