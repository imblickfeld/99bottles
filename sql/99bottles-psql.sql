-- (){ :;}; cat "$0" | sudo su postgres -c "psql -f -"; exit                                         
-- PostgreSQL --                                                                                     
                                                                                                     
SELECT lyrics AS "99 Bottles of Beer" FROM (                                                         
    SELECT i, 0 AS line, format('%s bottle%s of beer on the wall,', i, CASE WHEN i = 1 THEN '' ELSE  's' END) AS lyrics FROM generate_series(99,1,-1) i                                                   
    UNION ALL                                                                                        
    SELECT i, 1, format('%s bottle%s of beer.', i, CASE WHEN i = 1 THEN '' ELSE 's' END) FROM generate_series(99,1,-1) i                                                                           
    UNION ALL                                                                                        
    SELECT i, 2, 'Take one down and pass it around,' FROM generate_series(99,1,-1) i                 
    UNION ALL                                                                                        
    SELECT i + 1, 3, format('%s bottle%s of beer on the wall.', CASE WHEN i = 0 THEN 'No more' ELSE  CAST(i AS TEXT) END, CASE WHEN i = 1 THEN '' ELSE 's' END) FROM generate_series(98,0,-1) i           
    UNION ALL                                                                                        
    SELECT i, 4, '' FROM generate_series(99,1,-1) i                                                  
) t                                                                                                  
ORDER BY i DESC, line;