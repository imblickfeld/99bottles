-- (){ :;}; cat "$0" | sqlite3 /dev/null; exit
-- SQLite --

BEGIN TRANSACTION;

WITH RECURSIVE
    cnt(i, b) AS (VALUES(1, ' bottle') UNION ALL SELECT i + 1, ' bottles' FROM cnt WHERE i < 99)
SELECT lyrics FROM (
    SELECT i, 0 as line, i || b || ' of beer on the wall,' as lyrics FROM cnt
    UNION ALL
    SELECT i, 1, i || b || ' of beer.' FROM cnt
    UNION ALL
    SELECT i, 2, 'Take one down and pass it around,' FROM cnt
    UNION ALL
    SELECT i, 3 as line, CASE i WHEN 1 THEN 'No more' ELSE i - 1 END || ' bottle' || CASE i WHEN 2 THEN '' ELSE 's' END || ' of beer on the wall.' as lyrics FROM cnt
) t
ORDER BY i DESC, line;

ROLLBACK TRANSACTION;
