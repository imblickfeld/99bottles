                                        ; Console Message, 64 bit. V1.03
NULL              EQU 0                 ; Constants
STD_OUTPUT_HANDLE EQU -11

extern GetStdHandle                     ; Import external symbols
extern WriteFile                        ; Windows API functions, not decorated
extern ExitProcess

global Start                            ; Export symbols. The entry point

section .data                           ; Initialized data segment
    nomore      DB "No more"
    L_nomore    EQU $-nomore            ; Address of this line ($) - address of Message
    bottle      DB " bottle"
    L_bottle    EQU $-bottle
    ofbeer      DB " of beer"
    L_ofbeer    EQU $-ofbeer
    onthewall   DB " on the wall"
    L_onthewall EQU $-onthewall
    take        DB "Take one down and pass it around,", 0Dh, 0Ah
    L_take      EQU $-take

section .bss                            ; Uninitialized data segment
alignb 8
    hStdOut     resq 1
    n           resq 1

section .text
Start:
    SUB   RSP, 8                        ; Align the stack to a multiple of 16 bytes

    SUB   RSP, 32                       ; 32 bytes of shadow space
    MOV   ECX, STD_OUTPUT_HANDLE
    CALL  GetStdHandle
    MOV   qword [REL hStdOut], RAX
    ADD   RSP, 32                       ; Remove the 32 bytes

    SUB   RSP, 32 + 8 + 8               ; Shadow space + 5th parameter + align stack
                                        ; to a multiple of 16 bytes
    MOV   RCX, qword [REL hStdOut]      ; 1st parameter

    ; for (R11 == 99; R11 > 0; ++R11)
    LEA   R11, 99
Loop:

    MOV   AL, 3

IfAL:
    DEC   AL
    PUSH  RAX
    ; if (AL == 2) "XX bottles of beer on the wall,\r\n"
    ; if (AL == 1) "XX bottles of beer.\r\n Take one down and pass it around,\r\n"
    ; if (AL == 0) "XX-1 bottles of beer on the wall.\r\n\r\n"

    CALL  PrintInt                      ; print R11

    ; print " bottle"
    LEA   RDX, [REL bottle]
    LEA   R8, L_bottle
    CALL  Print

    ; if (R11 == 1) print 's'
    CMP   R11, 1
    JZ    OfBeer
    LEA   RAX, 's'
    MOV   QWORD [REL n], RAX
    LEA   RDX, [REL n]
    LEA   R8, 1
    CALL  Print

OfBeer:
    ; print " of beer"
    LEA   RDX, [REL ofbeer]
    LEA   R8, L_ofbeer
    CALL  Print

    POP   RAX
    CMP   AL, 1
    JZ    IfAL1
    PUSH  RAX

    ; print " on the wall"
    LEA   RDX, [REL onthewall]
    LEA   R8, L_onthewall
    CALL  Print

    POP   RAX
    TEST  AL, AL
    JZ    IfAL0
    PUSH  RAX

    ; print ",\r\n"
    LEA   RAX, 0D0A2Ch
    MOV   QWORD [REL n], RAX
    LEA   RDX, [REL n]
    LEA   R8, 3
    CALL  Print

    POP   RAX
    JMP   IfAL

IfAL1:
    ; print ".\r\n"
    LEA   RAX, 0D0A2Eh
    MOV   QWORD [REL n], RAX
    LEA   RDX, [REL n]
    LEA   R8, 3
    CALL  Print

    ; print "Take one down and pass it around,"
    LEA   RDX, [REL take]
    LEA   R8, L_take
    CALL  Print

    DEC   R11
    JMP   IfAL

IfAL0:
    ; print ".\r\n\r\n"
    LEA   RAX, 0D0A0D2Eh
    MOV   QWORD [REL n], RAX
    MOV   AL, 0Ah
    MOV   [REL n + 4], AL
    LEA   RDX, [REL n]
    LEA   R8, 5
    CALL  Print

    TEST  R11, R11
    JNZ   Loop
    ; endfor

    ; exit(0)
    XOR   ECX, ECX
    CALL  ExitProcess

; PrintInt(R11)
;     R11 = int to print, R11 < 100
PrintInt:
    TEST  R11, R11
    JNZ   PI_PrintNo
    LEA   RDX, [REL nomore]
    LEA   R8, L_nomore
    JMP   PI_DoPrint
PI_PrintNo:
    XOR   RDX, RDX
    MOV   RAX, R11
    MOV   R10, 10
    DIV   R10                           ; RAX = RAX|RDX / R10, RDX = RAX|RDX % R10
    LEA   R10, [REL n]                  ; R10 = @n
    LEA   R8, 1
    TEST  RAX, RAX
    JZ PI_AfterFirstChar                ; if (RAX == 0) don't print RAX
    ADD   RAX, 30h
    MOV   [R10], RAX
    INC   R10
    LEA   R8, 2
PI_AfterFirstChar
    ADD   DX, 30h
    MOV   [R10], DX
    LEA   RDX, [REL n]
PI_DoPrint:
    CALL  Print
    RET

; Print(RDX, R8)
;     RDX = text to print
;     R8  = length of text
Print:
    PUSH  R11                           ; WriteFile will alter R11
    SUB   RSP, 32 + 8 + 8               ; Shadow space + 5th parameter + align stack
                                        ; to a multiple of 16 bytes
    MOV   RCX, QWORD [REL hStdOut]      ; 1st parameter
                                        ; 2nd parameter = RDX
                                        ; 3rd parameter = r8
    LEA   R9, NULL                      ; 4th parameter
    MOV   QWORD [RSP + 4 * 8], NULL     ; 5th parameter
    CALL  WriteFile                     ; Output can be redirect to a file using >
    ADD   RSP, 48                       ; Remove the 48 bytes
    POP   R11
    RET