section .text

global _start

_start:
    ; for (rbx = 99; rbx > 0; ++rbx)
    mov     rbx, 99
forhead:
    test    rbx, rbx
    jz      forexit

    ; print "N bottle(s) of beer on the wall,"
    call    print_rbx

    lea     rsi, [bottle]
    lea     rdx, L_bottle
    call    print

    call    print_s

    lea     rsi, [ofbeer]
    lea     rdx, L_ofbeer
    call    print

    lea     rsi, [onthewall]
    lea     rdx, L_onthewall
    call    print

    mov     rax, 0x0a2c     ; ",\n"
    mov     rdx, 2
    call    print_rax

    ; print "N bottle(s) of beer."
    call    print_rbx

    lea     rsi, [bottle]
    lea     rdx, L_bottle
    call    print

    call    print_s

    lea     rsi, [ofbeer]
    lea     rdx, L_ofbeer
    call    print

    mov     rax, 0x0a2e     ; ".\n"
    mov     rdx, 2
    call    print_rax

    ; print "Take on down and pass it around."
    lea     rsi, [take]
    lea     rdx, L_take
    call    print

    ; print "N-1 bottle(s) of beer on the wall."
    dec     rbx
    call    print_rbx

    lea     rsi, [bottle]
    lea     rdx, L_bottle
    call    print

    call    print_s

    lea     rsi, [ofbeer]
    lea     rdx, L_ofbeer
    call    print

    lea     rsi, [onthewall]
    lea     rdx, L_onthewall
    call    print

    mov     rax, 0x0a0a2e     ; ".\n\n"
    mov     rdx, 3
    call    print_rax

    jmp     forhead
forexit:

exit:
    xor     rsi, rsi        ; errcode = 0
    mov     rax, 0x3c       ; syscall exit
    syscall

; print(rsi, rdx)
;   [rsi] = text to print
;   rdx   = len(text)
print:
    xor     rdi, rdi        ; print to stdout
    mov     rax, 1          ; syscall write
    syscall
    ret

; prints the string representation of rax
; print(rax, rdx)
;   rax = text to print
;   rdx = len(text)
print_rax:
    mov     [str], rax
    lea     rsi, [str]
    call    print
    ret

; prints the int value in rbx
print_rbx:
    test    rbx, rbx
    jz      _print_rbx_nomore

    mov     rax, rbx
    xor     rdx, rdx
    mov     qword [str], rdx    ; empty str first
    mov     r10, 10
    div     r10             ; rax = rax|rdx / r10, rdx = rax|rdx % r10
    xor     r10, r10        ; len(rsi - 1)
    test    rax, rax
    jz      _print_rbx_lo
    ; higher digit
    inc     r10
    add     al, 0x30
    mov     byte [str], al
    lea     rsi, [str]
    inc     r10
_print_rbx_lo:
    ; lower digit
    add     dl, 0x30
    mov     byte [str+r10], dl
    lea     rsi, [str]
    inc     r10
    mov     rdx, r10
    jmp     _print_rbx_do
_print_rbx_nomore:
    lea     rsi, [nomore]
    mov     rdx, L_nomore
    jmp     _print_rbx_do
_print_rbx_do:
    call    print
    ret
    

; if (rbx != 1) print "s"
print_s:
    cmp     rbx, 1
    jz      _print_s_ret
    mov     rax, 's'
    mov     rdx, 1
    call    print_rax
_print_s_ret:
    ret


section .bss
    str:        resq 1      ; string to print

section .rodata
    nomore      db "No more"
    L_nomore    equ $-nomore            ; address of this line ($) - address of Message
    bottle      db " bottle"
    L_bottle    equ $-bottle
    ofbeer      db " of beer"
    L_ofbeer    equ $-ofbeer
    onthewall   db " on the wall"
    L_onthewall equ $-onthewall
    take        db "Take one down and pass it around,", 0x0a
    L_take      equ $-take


