# Scratch

To edit and/or run this program, try it [here](https://scratch.mit.edu/projects/editor/) online: upload the [file](99bottles.sb3) via *File &rarr; Load from your computer* and then run the program by clicking the green flag.

![Screenshot](screenshot.png "Screenshot")
