﻿<#

.SYNOPSIS
    Get-99Bottles prints the songtext of the "99 bottles" song.

.DESCRIPTION
    ... because it is cool.

.PARAMETER Bottles
    Number of bottles of beer on the wall.

.LINK
    https://www.rosettacode.org/wiki/99_Bottles_of_Beer/

.NOTES
    FileName: Get-99Bottles.ps1
    Author: Klaus Bergmann

.EXAMPLE
    ./Get-99Bottles

.EXAMPLE
    ./Get-99Bottles -Bottles 99

.EXAMPLE
    echo 99 | ./Get-99Bottles

#>

[cmdletbinding()]
param(
    [Parameter(Mandatory = $false, ValueFromPipeline)]
    [int]$Bottles = 99
)
process {
    for ($i = $Bottles; $i -ge 0; --$i) {
        if ($i -lt $Bottles) {
            (&{ if($i -eq 0) {"No more"} else {[string]$i} }) `
                + " bottle" `
                + (&{ if ($i -ne 1) { "s" } }) `
                + " of beer on the wall.`n"
        }
        if ($i -eq 0) {
            break
        }
        foreach ($j in 0..1) {
            (&{ if($i -eq 0) {"No more"} else {[string]$i} }) `
                + " bottle" `
                + (&{ if ($i -ne 1) { "s" } }) `
                + " of beer" `
                + (&{ if ($j -eq 0) { " on the wall," } else { "." } })
        }
        "Take one down, pass it around,"
    }
}