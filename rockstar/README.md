# Rockstar

To run this code, try it [here](https://codewithrockstar.com/online). (Insert the code over there and click "Rock!")

If you want to try your own, you may want to have a look at their [docs](https://codewithrockstar.com/docs).