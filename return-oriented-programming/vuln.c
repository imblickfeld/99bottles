#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <limits.h>

int sing;
char* answer;

int ask() {
    char buf[2];
    printf("Do you want me to sing a beer song? [Y/N] ");
    fflush(stdout);
    read(STDIN_FILENO, buf, INT_MAX);
    return buf[0] == 'Y' || buf[0] == 'y';
}

int main() {
    sing = ask();
    answer = sing ? "\"La la, bottles of beer on the wall, la la. Take one down and pass it around, la la ...\" \n\nNah, I don't like beer!" : "OK, I'll be quiet.";
    puts(answer);
    exit(0);
}