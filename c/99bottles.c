#include <stdio.h>

int main(void) {
    const char N = 99;
    for (char i = N; i >= 0; --i) {
        char s = i == 1 ? 0 : 's';
        if (i < N) {
            if (i == 0)
                printf("No more");
            else
                printf("%d", i);
            printf(" bottle%c of beer on the wall.\n", s);
            if (i == 0)
                break;
            printf("\n");
        }
        printf("%d bottle%c of beer on the wall,\n", i, s);
        printf("%d bottle%c of beer.\n", i, s);
        printf("Take one down and pass it around,\n");
    }
    return 0;
}
