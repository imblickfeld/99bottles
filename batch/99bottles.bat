@echo off
setlocal enabledelayedexpansion
set bottles=bottles
for /L %%i in (99,-1,1) do (
	if %%i equ 1 set bottles=bottle
	if %%i lss 99 (
		echo %%i !bottles! of beer on the wall.
		echo.
	)
	echo %%i !bottles! of beer on the wall,
	echo %%i !bottles! of beer.
	echo Take one down and pass it around,
)
echo No more bottles of beer on the wall.