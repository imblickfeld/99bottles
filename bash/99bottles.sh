#!/bin/bash

N=99
i=$N
while [ 1 ]; do
#for i in 5 .. 1; do
    bottle="bottles"
    if [ "$i" -eq "1" ]; then
        bottle="bottle"
    fi
    if [ "$i" -ne "$N" ]; then
        b=$i
        if [ "$i" -eq "0" ]; then
            b="No more"
        fi
        echo -e "$b $bottle of beer on the wall.\n"
    fi
    if [ "$i" -eq "0" ]; then
        exit
    fi
    echo "$i $bottle of beer on the wall,"
    echo "$i $bottle of beer."
    echo "Take one down and pass it around,"
    ((i-=1))
done
